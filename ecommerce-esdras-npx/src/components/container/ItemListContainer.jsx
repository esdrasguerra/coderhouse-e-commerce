import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import { gFetch } from "../../helper/bDat";
import { Spinner } from "react-bootstrap";
import ItemList from "../ItemList/ItemList";

const ItemListContainer = () => {
  const [productos, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    gFetch
      .then((resp) => {
        setProducts(resp);
      })
      .catch((rej) => {
        console.log(rej);
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  return loading ? (
    <div className="text-center mt-4">
      <p>Cargando viajes, porfavor espere...</p>
      <Spinner animation="border" role="status" variant="info" />
    </div>
  ) : (
    <ItemList prod={productos} />
  );
};

export default ItemListContainer;