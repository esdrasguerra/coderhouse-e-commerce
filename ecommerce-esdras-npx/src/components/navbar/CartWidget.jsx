/*import React from 'react'*/
import logo from "../../images/logo.svg"

export const CartWidget = () => {
  return (
    <div class="w-25 text-lg-end">
      <img src="https://cdn-icons-png.flaticon.com/512/263/263142.png" alt="CartWidget" class="w-25"/>
    </div>
  )
}

export default CartWidget
