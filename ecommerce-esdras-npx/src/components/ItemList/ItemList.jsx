import {Container} from "react-bootstrap";
import Item from "../Item/Item";

function ItemList({prod}) {
  return (
    <Container className="d-flex flex-wrap w-100">
      {prod.map((elemento) => (
        <Item productos={elemento} key={elemento.id} />
      ))}
    </Container>
  );
}

export default ItemList;