import React from 'react'
import {Card, Button} from "react-bootstrap"

function Item({productos}) {
    const {id, title, img, pasajeros, precio} = productos

    return(
        <div className="m-3 col-3">
            <Card >
                <Card.Img variant="top" src={img} />
                <Card.Body>
                    <Card.Text className="mb-0">{precio}</Card.Text>
                    <Card.Title className="mb-0">
                        {title}
                    </Card.Title>
                    <Card.Title className="mb-4">
                        {pasajeros} Persona
                    </Card.Title>
                    <Button variant="primary">Buy</Button>
                </Card.Body>
            </Card>
      </div>
    )
}

export default Item
