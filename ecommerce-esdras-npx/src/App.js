// 1 - Librerías y dependencias en uso
import React, { Component } from 'react'

// 2 - Componentes
import NavBar from './components/navbar/navbar'
import ItemListContainer from './components/container/ItemListContainer'

// 3 - Estilos
import 'bootstrap/dist/css/bootstrap.min.css';

export default class App extends Component {
  render() {
    return (
      <div>
        <NavBar />
        <ItemListContainer saludo="Hola soy Item List Container">
          
        </ItemListContainer>

      </div>
    )
  }
}

